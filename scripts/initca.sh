
peer channel create -o orderer.cdg.co.th:7050 -c cdg -f ./channel-artifacts/channel-cdg.tx 
peer channel join -b cdg.block

CORE_PEER_ADDRESS=peer1.cdt.cdg.co.th:7051

peer channel join -b cdg.block


peer chaincode install -n log -v 1.0 -l node -p /opt/gopath/src/github.com/chaincode/logger/

CORE_PEER_ADDRESS=peer1.cdt.cdg.co.th:7051

peer chaincode instantiate -o orderer.cdg.co.th:7050 -n log -v 1.0 -l node -C cdg -c '{"Args":[]}' -P "OR ('CdtMSP.member','GableMSP.member')"

peer chaincode invoke -o orderer.cdg.co.th:7050 -C cdg -n log -c '{"function":"initLog","Args":[]}'

