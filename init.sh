./cryptogen generate --config=./crypto-config.yaml
./configtxgen -profile TwoOrgsOrdererGenesis -outputBlock ./channel-artifacts/genesis.block
./configtxgen -profile TwoOrgsChannel -outputCreateChannelTx ./channel-artifacts/channel-cdg.tx -channelID cdg

sleep 2 

IMAGE_TAG="latest" docker-compose -f docker-compose-cli.yaml up -d 2>&1
