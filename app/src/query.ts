/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { FileSystemWallet, Gateway } from 'fabric-network';
import * as fs from 'fs';
import * as path from 'path';

const ccpPath = path.resolve(__dirname, '..', 'connection.json');
const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
const ccp = JSON.parse(ccpJSON);

async function main() {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = new FileSystemWallet(walletPath);
    console.log(`Wallet path: ${walletPath}`);

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists('user2');
    if (!userExists) {
        console.log('An identity for the user "user1" does not exist in the wallet');
        console.log('Run the registerUser.ts application before retrying');
        return;
    }
    // Create a new gateway for connecting to our peer node.
    try {
        const gateway = new Gateway();
        await gateway.connect(ccp, { wallet, identity: 'user2', discovery: { enabled: false } });
        try {
            const network = await gateway.getNetwork('cdg');
            const contract = network.getContract('log2');
            const result = await contract.evaluateTransaction('getByKey',"4");
    console.log(`Transaction has been evaluated, result is: ${result.toString()}`);
        } catch (error) {
            console.log("1");
            console.log(error.message);
        }
    } catch (error) {
        console.log("2");
        console.log(error.message);

    }

    // Get the network (channel) our contract is deployed to.

    // Get the contract from the network.
    // const contract = network.getContract('fabcar');

    // Evaluate the specified transaction.
    // queryCar transaction - requires 1 argument, ex: ('queryCar', 'CAR4')
    // queryAllCars transaction - requires no arguments, ex: ('queryAllCars')
    

}

main();
