
export interface ILog {
    name: string
    username: string
    function: string
    timeStampWhenDoFunction: string
}

export class Log {

    public name: string
    public username: string
    public function: string
    public timeStampWhenDoFunction: string

    constructor(info: ILog) {
        this.name = info.name
        this.function = info.function
        this.username = info.username
        this.timeStampWhenDoFunction = info.timeStampWhenDoFunction
    }

    public returnLog() {
        return {
            name: this.name,
            function: this.function,
            timeStampWhenDoFunction: this.timeStampWhenDoFunction,
            username: this.username
        }
    }
}