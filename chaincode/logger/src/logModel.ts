import { Context, Contract } from 'fabric-contract-api';
import { Log, ILog } from './log';

export class LogModel extends Contract {

    public key = 1

    public async getByKey(ctx: Context, key: string) {

        const result = await ctx.stub.getState(key)
        if (!result || result.length === 0) {
            throw new Error(`${key} dose not exist`)
        }
        console.log('result --> ' + result.toString());
        return result.toString()
    }

    public async getAll(ctx: Context, start: string, end: string) {
        console.log(`=========== get log processing ============`);
        try {
            const iterator = await ctx.stub.getStateByRange(start, end)
            const results = []
            while (true) {
                const res = await iterator.next()
                if (res.value && res.value.value.toString()) {
                    console.log('data --> ' + res.value.value.toString('utf8'));
                    const key = res.value.key
                    let Record: any
                    try {
                        Record = JSON.parse(res.value.value.toString('utf8'));
                    } catch (err) {
                        console.log(err);
                        Record = res.value.value.toString('utf8');
                    }
                    results.push({
                        key,
                        record: Record
                    })
                }

                if (res.done) {
                    console.log("query done!");
                    await iterator.close()
                    console.log(results);
                    return JSON.stringify(results)
                }
            }
        } catch (error) {
            console.log("=============== Something Wrong =================");
            console.log('Error : ' + error);
        }
    }

    public async createLog(ctx: Context, log: ILog) {
        console.log("========== START : Create Log ===========");
        const newLog = new Log(log)
        console.log("latest key : ", ++this.key);
        try {
            await ctx.stub.putState(this.key.toString(), Buffer.from(JSON.stringify(newLog.returnLog())))
        } catch (error) {
            console.log(`=========== Error =========`);
            console.log(error);
        }
    }
}