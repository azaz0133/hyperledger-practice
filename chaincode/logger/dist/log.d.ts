export interface ILog {
    name: string;
    username: string;
    function: string;
    timeStampWhenDoFunction: string;
}
export declare class Log {
    name: string;
    username: string;
    function: string;
    timeStampWhenDoFunction: string;
    constructor(info: ILog);
    returnLog(): {
        name: string;
        function: string;
        timeStampWhenDoFunction: string;
        username: string;
    };
}
