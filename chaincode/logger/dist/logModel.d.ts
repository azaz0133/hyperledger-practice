import { Context, Contract } from 'fabric-contract-api';
import { ILog } from './log';
export declare class LogModel extends Contract {
    key: number;
    getByKey(ctx: Context, key: string): Promise<string>;
    getAll(ctx: Context, start: string, end: string): Promise<string>;
    createLog(ctx: Context, log: ILog): Promise<void>;
}
