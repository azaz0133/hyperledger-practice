"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Log {
    constructor(info) {
        this.name = info.name;
        this.function = info.function;
        this.username = info.username;
        this.timeStampWhenDoFunction = info.timeStampWhenDoFunction;
    }
    returnLog() {
        return {
            name: this.name,
            function: this.function,
            timeStampWhenDoFunction: this.timeStampWhenDoFunction,
            username: this.username
        };
    }
}
exports.Log = Log;
//# sourceMappingURL=log.js.map