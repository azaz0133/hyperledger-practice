"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fabric_contract_api_1 = require("fabric-contract-api");
const log_1 = require("./log");
class LogModel extends fabric_contract_api_1.Contract {
    constructor() {
        super(...arguments);
        this.key = 1;
    }
    async getByKey(ctx, key) {
        const result = await ctx.stub.getState(key);
        if (!result || result.length === 0) {
            throw new Error(`${key} dose not exist`);
        }
        console.log('result --> ' + result.toString());
        return result.toString();
    }
    async getAll(ctx, start, end) {
        console.log(`=========== get log processing ============`);
        try {
            const iterator = await ctx.stub.getStateByRange(start, end);
            const results = [];
            while (true) {
                const res = await iterator.next();
                if (res.value && res.value.value.toString()) {
                    console.log('data --> ' + res.value.value.toString('utf8'));
                    const key = res.value.key;
                    let Record;
                    try {
                        Record = JSON.parse(res.value.value.toString('utf8'));
                    }
                    catch (err) {
                        console.log(err);
                        Record = res.value.value.toString('utf8');
                    }
                    results.push({
                        key,
                        record: Record
                    });
                }
                if (res.done) {
                    console.log("query done!");
                    await iterator.close();
                    console.log(results);
                    return JSON.stringify(results);
                }
            }
        }
        catch (error) {
            console.log("=============== Something Wrong =================");
            console.log('Error : ' + error);
        }
    }
    async createLog(ctx, log) {
        console.log("========== START : Create Log ===========");
        const newLog = new log_1.Log(log);
        console.log("latest key : ", ++this.key);
        try {
            await ctx.stub.putState(this.key.toString(), Buffer.from(JSON.stringify(newLog.returnLog())));
        }
        catch (error) {
            console.log(`=========== Error =========`);
            console.log(error);
        }
    }
}
exports.LogModel = LogModel;
//# sourceMappingURL=logModel.js.map